import React from 'react';

const Home = (props) => {
    if (props.name) {
        return <div>Hello, {props.name}</div>;
    } else {
        return <span>Hey, stranger</span>;
    }
};

export default Home;