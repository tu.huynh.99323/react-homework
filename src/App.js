import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import React, { Suspense, lazy } from 'react';
import './App.css';

const Home = lazy(() => import('./pages/Home')),
  About = lazy(() => import('./pages/About')),
  ProductDetail = lazy(() => import('./pages/ProductDetail')),
  UserProfile = lazy(() => import('./pages/UserProfile')),
  ErrorPage = lazy(() => import('./pages/UserProfile'));
const App = () => (
  <Router>
    <Suspense fallback={<div>Loading...</div>} >
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/product/:id" component={ProductDetail} />
        <PrivateRoute path="/profile" component={UserProfile}/>
        <Route component={ErrorPage}/>
      </Switch>
    </Suspense>
  </Router>
);
export default App;
